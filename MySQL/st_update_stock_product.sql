-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: customer-service
-- ------------------------------------------------------
-- Server version	8.0.19
USE `customer-service`;
DELIMITER //
CREATE PROCEDURE st_update_stock_product(
       p_codigo        CHAR(13),
       p_cantidad      INT
       )

    BEGIN
        DECLARE c_total  INT DEFAULT 0;

        -- Verificamos si existe el codigo a actualizar.
        
        IF (SELECT count(*) FROM product WHERE codigo = p_codigo) > 0 THEN
            BEGIN
            -- Obtenemos la cantidad actual de product


                SET c_total = (SELECT cantidad FROM product WHERE codigo = p_codigo);
                SET c_total =c_total + p_cantidad;
        
                -- Actualizamos
                    IF c_total > 0 
                        THEN
                            BEGIN
                                UPDATE product SET cantidad = c_total WHERE codigo = p_codigo;
                            END;
                    ELSE
                        BEGIN
                            SIGNAL SQLSTATE '50400' SET MESSAGE_TEXT = 'La cantidad ingresada no es válida.';
                        END;
                    END IF;
            END;
        
    ELSE
        BEGIN
            SIGNAL SQLSTATE '50404' SET MESSAGE_TEXT = 'El código del producto no existe.';
        END;
    END IF;
END; //
       DELIMITER ;